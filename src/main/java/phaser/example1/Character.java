package phaser.example1;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class Character implements Runnable {
    protected final static AtomicInteger idSequence = new AtomicInteger();
    protected abstract int getId();

    @Override
    public void run() {
        try {
            System.out.format("%s вступил в игру%n", this);
            doSomething();
            System.out.format("%s умер%n", this);
        } catch (InterruptedException exception){
            System.out.println("Game was interrupted!");
        }
    }

    private void doSomething() throws InterruptedException {
            Thread.sleep(ThreadLocalRandom.current().nextInt(10, 100));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " #" + getId();
    }

}
