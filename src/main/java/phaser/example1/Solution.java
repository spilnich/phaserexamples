package phaser.example1;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Phaser;

public class Solution {
    public static void main(String[] args) {
        List<Character> characters = new ArrayList<>();
        characters.add(new Plant());
        characters.add(new Plant());
        characters.add(new Zombie());
        characters.add(new Zombie());
        characters.add(new Zombie());
        start(characters);
    }

    private static boolean isEveryoneReady = false;

    private static void start(List<Character> characters) {
        final var phaser = new Phaser(1 + characters.size());

        for (final var character : characters) {
            System.out.format("%s присоединился к игре%n", character);

            new Thread(() -> {
                System.out.format("%s готовится играть%n", character);
                phaser.arriveAndAwaitAdvance();
                    if (!isEveryoneReady) {
                        isEveryoneReady = true;
                        System.out.println("Игра началась!");
                    }
                    character.run();
            }).start();
        }
        phaser.arriveAndDeregister();
    }
}
