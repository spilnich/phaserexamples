package phaser.example2;

import java.util.ArrayList;
import java.util.List;

public class BusStop {
    private final int busStopId;
    private final String busStopName;
    private List<Passenger> passengers;

    public BusStop(int busStopId, String busStopName) {
        this.busStopId = busStopId;
        this.busStopName = busStopName;
        this.passengers = new ArrayList<>();
    }

    public int getBusStopId() {
        return busStopId;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers){
        this.passengers = passengers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BusStop busStop = (BusStop) o;

        return busStopId == busStop.busStopId;
    }

    @Override
    public int hashCode() {
        return busStopId;
    }

    @Override
    public String toString() {
        return "Остановка " + busStopId + ": " + busStopName;
    }
}
