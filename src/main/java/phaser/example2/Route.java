package phaser.example2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Phaser;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Route implements Runnable {
    private static final String OPEN = "\tДвери открылись";
    private static final String CLOSE = "\tДвери закрылись";
    private static final String TO_DEPO = "Автобус завершил маршрут";
    private static final String START = "Автобус выехал на маршрут";

    private final Phaser phaser;
    private final List<BusStop> busStops;

    private final Random random = new Random();

    public Route(Phaser phaser) {
        this.phaser = phaser;
        busStops = List.of(
                new BusStop(1, "Конева"),
                new BusStop(2, "30-лет Победы"),
                new BusStop(3, "Сумгаитская"),
                new BusStop(4, "Королева"),
                new BusStop(5, "Завод Богдан"),
                new BusStop(6, "ЗТА"),
                new BusStop(7, "Ильина"),
                new BusStop(8, "Любава")
        );
        generatePassengers(busStops);
    }

    @Override
    public void run() {
        startBus();
        busStops.forEach(this::driveByRoute);
        stopBus();
    }

    private void driveByRoute(BusStop busStop) {
        try {
        System.out.println(busStop);

        phaser.arriveAndAwaitAdvance();

        var awaited = busStop.getPassengers().stream()
                .filter(pass -> pass.getDeparture() == busStop)
                .map(Thread::new)
                .collect(Collectors.toList());

        phaser.bulkRegister(awaited.size());
        /*IntStream.range(0, awaited.size())
                .forEach(i -> phaser.register());*/

        System.out.println(OPEN);

        awaited.forEach(Thread::start);

            Thread.sleep(1000);

        System.out.println(CLOSE);

        } catch (InterruptedException exception) {
            System.out.println("Программа долго выполнялась и была прервана");
        }
    }

    private void stopBus() {
        System.out.println(TO_DEPO);
        phaser.arriveAndDeregister();
    }

    private void startBus() {
        System.out.println(START);
    }

    private void generatePassengers(List<BusStop> busStops){
        final var size = busStops.size();
        final var passId = new AtomicInteger(1);

        busStops.forEach( busStop -> {
            if (busStop.getBusStopId() < size ) {
                var count = random.nextInt(5);
                final var passengers = new ArrayList<Passenger>();
                while (count-- > 0) {
                    var departureId = random.nextInt(size - busStop.getBusStopId()) + busStop.getBusStopId();
                    passengers.add(
                            new Passenger(passId.getAndIncrement(), busStop, busStops.get(departureId), phaser)
                    );
                }
                busStop.setPassengers(passengers);
            }
        });
    }

    public static void main(String[] args) throws InterruptedException {
        var phaser = new Phaser(1);
        var route = new Route(phaser);
        var thread = new Thread(route);
        thread.start();
        thread.join(10000);
    }
}